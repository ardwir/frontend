package com.muhardin.endy.training.microservices.frontend.dto;

import lombok.Data;

@Data
public class Customer {
    private String id;
    private String email;
    private String nama;
    private String noHp;
    private String alamat;
    private String foto;
}
