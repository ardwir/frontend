package com.muhardin.endy.training.microservices.frontend.service;


import com.muhardin.endy.training.microservices.frontend.dto.Customer;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient("pembayaran")
public interface PembayaranClientService {
    @GetMapping("/api/customer/")
    Iterable<Customer> daftarCustomer();
}
